#pragma once

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "net/gcoap.h"
#include "od.h"
#include "fmt.h"

#ifndef COAP_RESOURCE
#define COAP_RESOURCE "/api/v1"
#endif

int coap_init(sock_udp_ep_t *remote);
size_t coap_send(uint8_t *buf, size_t len, sock_udp_ep_t *remote);
void coap_resp_handler(const gcoap_request_memo_t *memo, coap_pkt_t *pdu,
                       const sock_udp_ep_t *remote);
