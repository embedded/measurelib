#include "senml.h"
#include "math.h"
#include "saul_reg.h"

static uint8_t phydat_unit_to_senml_unit(uint8_t unit)
{
    switch (unit) {
    // Compatible units
    case UNIT_TEMP_C:  return SENML_UNIT_CELSIUS;
    case UNIT_TEMP_K:  return SENML_UNIT_KELVIN;
    case UNIT_LUX:     return SENML_UNIT_LUX;
    case UNIT_M:       return SENML_UNIT_METER;
    case UNIT_M2:      return SENML_UNIT_SQUARE_METER;
    case UNIT_M3:      return SENML_UNIT_CUBIC_METER;
    case UNIT_GR:      return SENML_UNIT_GRAM;
    case UNIT_A:       return SENML_UNIT_AMPERE;
    case UNIT_V:       return SENML_UNIT_VOLT;
    case UNIT_W:       return SENML_UNIT_WATT;
    case UNIT_COULOMB: return SENML_UNIT_COULOMB;
    case UNIT_F:       return SENML_UNIT_FARAD;
    case UNIT_OHM:     return SENML_UNIT_OHM;
    case UNIT_PH:      return SENML_UNIT_PH;
    case UNIT_PA:      return SENML_UNIT_PASCAL;
    case UNIT_CD:      return SENML_UNIT_CANDELA;

    // Compatible Secondary units
    case UNIT_DBM:     return SENML_UNIT_DECIBEL_MILLIWATT;
    case UNIT_PERCENT: return SENML_UNIT_PERCENT;
    case UNIT_PERMILL: return SENML_UNIT_PERMILLE;
    case UNIT_PPM:     return SENML_UNIT_PARTS_PER_MILLION;
    case UNIT_PPB:     return SENML_UNIT_PARTS_PER_BILLION;

    // RIOT extensions
    case UNIT_DPS:     return SENML_UNIT_DEGREE_PER_SECOND;
    case UNIT_CPM3:    return SENML_UNIT_COUNT_PER_CUBIC_METER;

    // Incompatible units
    case UNIT_TEMP_F:  return SENML_UNIT_NONE;  // use K or Cel instead
    case UNIT_GS:      return SENML_UNIT_NONE;  // use T instead
    case UNIT_G:       return SENML_UNIT_NONE;  // use m/s2 instead
    case UNIT_BAR:     return SENML_UNIT_NONE;  // use Pa or hPa instead
    case UNIT_TIME:    return SENML_UNIT_NONE;  // split into second/minute/hour
    case UNIT_DATE:    return SENML_UNIT_NONE;  // split into day/month/year
    case UNIT_GPM3:    return SENML_UNIT_NONE;  // use kg/m3 instead

    default:           return SENML_UNIT_NONE;
    }
}

void phydat_time_to_senml_value(senml_value_t *senml, const phydat_t *phydat,
                                const char *name, float age)
{
    senml->name = name;
    senml->age = age;
    senml->value = (float)(phydat->val[0] +
                           60 * phydat->val[1] +
                           3600 * phydat->val[2]);
    senml->unit = SENML_UNIT_SECOND;
}

void phydat_to_senml_bool(senml_bool_t *senml, const phydat_t *phydat,
                          const uint8_t dim, const char *name, float age)
{
    senml->name = name;
    senml->age = age;
    senml->value = phydat->val[dim] == 1;
    senml->unit = SENML_UNIT_NONE;
}

void phydat_to_senml_value(senml_value_t *senml, const phydat_t *phydat,
                           const uint8_t dim, const char *name, float age)
{
    senml->name = name;
    senml->age = age;

    float value = (float)(phydat->val[dim]);

    if (phydat->scale) {
        value *= pow(10, phydat->scale);
    }

    switch (phydat->unit) {
    // complex conversions
    case UNIT_TIME:
        senml->value = value;
        senml->unit = (dim == 0)
                      ? SENML_UNIT_SECOND
                      : (dim == 1)
                          ? SENML_UNIT_MINUTE
                          : SENML_UNIT_HOUR;
    case UNIT_DATE:
        senml->value = value;
        senml->unit = (dim == 0)
                      ? SENML_UNIT_DAY
                      : (dim == 1)
                         ? SENML_UNIT_MONTH
                         : SENML_UNIT_YEAR;

    // simple conversions
    case UNIT_F:
        senml->value = (value + 459.67) * (5 / 9);
        senml->unit = SENML_UNIT_KELVIN;
        break;
    case UNIT_G:
        senml->value = 9.80665 * value;
        senml->unit = SENML_UNIT_METER_PER_SQUARE_SECOND;
        break;
    case UNIT_BAR:
        senml->value = 100000 * value;
        senml->unit = SENML_UNIT_PASCAL;
        break;
    case UNIT_GPM3:
        senml->value = 0.001 * value;
        senml->unit = SENML_UNIT_KILOGRAM_PER_CUBIC_METER;
        break;
    case UNIT_GS:
        senml->value = 0.0001 * value;
        senml->unit = SENML_UNIT_TESLA;
        break;

    // compatible, or not converted
    default:
        senml->value = value;
        senml->unit = phydat_unit_to_senml_unit(phydat->unit);
        break;
    }
}

const char *senml_unit_to_str(uint8_t unit)
{
    switch (unit) {
    case SENML_UNIT_NONE:                         return "";
    case SENML_UNIT_METER:                        return "m";
    case SENML_UNIT_KILOGRAM:                     return "kg";
    case SENML_UNIT_GRAM:                         return "g";
    case SENML_UNIT_SECOND:                       return "s";
    case SENML_UNIT_AMPERE:                       return "A";
    case SENML_UNIT_KELVIN:                       return "K";
    case SENML_UNIT_CANDELA:                      return "cd";
    case SENML_UNIT_MOLE:                         return "mol";
    case SENML_UNIT_HERTZ:                        return "Hz";
    case SENML_UNIT_RADIAN:                       return "rad";
    case SENML_UNIT_STERADIAN:                    return "sr";
    case SENML_UNIT_NEWTON:                       return "N";
    case SENML_UNIT_PASCAL:                       return "Pa";
    case SENML_UNIT_JOULE:                        return "J";
    case SENML_UNIT_WATT:                         return "W";
    case SENML_UNIT_COULOMB:                      return "C";
    case SENML_UNIT_VOLT:                         return "V";
    case SENML_UNIT_FARAD:                        return "F";
    case SENML_UNIT_OHM:                          return "Ohm";
    case SENML_UNIT_SIEMENS:                      return "S";
    case SENML_UNIT_WEBER:                        return "Wb";
    case SENML_UNIT_TESLA:                        return "T";
    case SENML_UNIT_HENRY:                        return "H";
    case SENML_UNIT_CELSIUS:                      return "Cel";
    case SENML_UNIT_LUMEN:                        return "lm";
    case SENML_UNIT_LUX:                          return "lx";
    case SENML_UNIT_BECQUEREL:                    return "Bq";
    case SENML_UNIT_GRAY:                         return "Gy";
    case SENML_UNIT_SIEVERT:                      return "Sv";
    case SENML_UNIT_KATAL:                        return "kat";
    case SENML_UNIT_SQUARE_METER:                 return "m2";
    case SENML_UNIT_CUBIC_METER:                  return "m3";
    case SENML_UNIT_LITER:                        return "l";
    case SENML_UNIT_METER_PER_SECOND:             return "m/s";
    case SENML_UNIT_METER_PER_SQUARE_SECOND:      return "m/s2";
    case SENML_UNIT_CUBIC_METER_PER_SECOND:       return "m3/s";
    case SENML_UNIT_LITER_PER_SECOND:             return "l/s";
    case SENML_UNIT_WATT_PER_SQUARE_METER:        return "W/m2";
    case SENML_UNIT_CANDELA_PER_SQUARE_METER:     return "cd/m2";
    case SENML_UNIT_BIT:                          return "bit";
    case SENML_UNIT_BIT_PER_SECOND:               return "bit/s";
    case SENML_UNIT_LATITUDE:                     return "lat";
    case SENML_UNIT_LONGITUDE:                    return "lon";
    case SENML_UNIT_PH:                           return "pH";
    case SENML_UNIT_DECIBEL:                      return "dB";
    case SENML_UNIT_DBW:                          return "dBW";
    case SENML_UNIT_BEL:                          return "Bspl";
    case SENML_UNIT_COUNT:                        return "count";
    case SENML_UNIT_RATIO:                        return "/";
    case SENML_UNIT_RATIO_2:                      return "%";
    case SENML_UNIT_RELATIVE_HUMIDITY_PERCENT:    return "%RH";
    case SENML_UNIT_REMAINING_BATTERY_PERCENT:    return "%EL";
    case SENML_UNIT_REMAINING_BATTERY_SECONDS:    return "EL";
    case SENML_UNIT_RATE:                         return "1/s";
    case SENML_UNIT_RPM:                          return "1/min";
    case SENML_UNIT_HEART_RATE:                   return "beat/min";
    case SENML_UNIT_HEART_BEATS:                  return "beats";
    case SENML_UNIT_SIEMENS_PER_METER:            return "S/m";

    case SENML_UNIT_BYTE:                         return "B";
    case SENML_UNIT_VOLT_AMPERE:                  return "VA";
    case SENML_UNIT_VOLT_AMPERE_SECOND:           return "VAs";
    case SENML_UNIT_VOLT_AMPERE_REACTIVE:         return "var";
    case SENML_UNIT_VOLT_AMPERE_REACTIVE_SECOND:  return "vars";
    case SENML_UNIT_JOULE_PER_METER:              return "J/m";
    case SENML_UNIT_KILOGRAM_PER_CUBIC_METER:     return "kg/m3";
    case SENML_UNIT_DEGREE:                       return "deg";

    case SENML_UNIT_NEPHELOMETRIC_TURBIDITY_UNIT: return "NTU";

    case SENML_UNIT_MILLISECOND:                  return "ms";
    case SENML_UNIT_MINUTE:                       return "min";
    case SENML_UNIT_HOUR:                         return "h";
    case SENML_UNIT_MEGAHERTZ:                    return "MHz";
    case SENML_UNIT_KILOWATT:                     return "kW";
    case SENML_UNIT_KILOVOLT_AMPERE:              return "kVA";
    case SENML_UNIT_KILOVAR:                      return "kvar";
    case SENML_UNIT_AMPERE_HOUR:                  return "Ah";
    case SENML_UNIT_WATT_HOUR:                    return "Wh";
    case SENML_UNIT_KILOWATT_HOUR:                return "kWh";
    case SENML_UNIT_VAR_HOUR:                     return "varh";
    case SENML_UNIT_KILOVAR_HOUR:                 return "kvarh";
    case SENML_UNIT_KILOVOLT_AMPERE_HOUR:         return "kVAh";
    case SENML_UNIT_WATT_HOUR_PER_KILOMETER:      return "Wh/km";
    case SENML_UNIT_KIBIBYTE:                     return "KiB";
    case SENML_UNIT_GIGABYTE:                     return "GB";
    case SENML_UNIT_MEGABIT_PER_SECOND:           return "MBit/s";
    case SENML_UNIT_BYTE_PER_SECOND:              return "B/s";
    case SENML_UNIT_MEGABYTE_PER_SECOND:          return "MB/s";
    case SENML_UNIT_MILLIVOLT:                    return "mV";
    case SENML_UNIT_MILLIAMPERE:                  return "mA";
    case SENML_UNIT_DECIBEL_MILLIWATT:            return "dBm";
    case SENML_UNIT_MICROGRAM_PER_CUBIC_METER:    return "ug/m3";
    case SENML_UNIT_MILLIMETER_PER_HOUR:          return "mm/h";
    case SENML_UNIT_METER_PER_HOUR:               return "m/h";
    case SENML_UNIT_PARTS_PER_MILLION:            return "ppm";
    case SENML_UNIT_PERCENT:                      return "/100";
    case SENML_UNIT_PERMILLE:                     return "/1000";
    case SENML_UNIT_HECTOPASCAL:                  return "hPa";
    case SENML_UNIT_MILLIMETER:                   return "mm";
    case SENML_UNIT_CENTIMETER:                   return "cm";
    case SENML_UNIT_KILOMETER:                    return "km";
    case SENML_UNIT_KILOMETER_PER_HOUR:           return "km/h";
    case SENML_UNIT_PARTS_PER_BILLION:            return "ppb";
    case SENML_UNIT_PARTS_PER_TRILLION:           return "ppt";
    case SENML_UNIT_VOLT_AMPERE_HOUR:             return "VAh";
    case SENML_UNIT_MILLIGRAM_PER_LITER:          return "mg/l";
    case SENML_UNIT_MICROGRAM_PER_LITER:          return "ug/l";
    case SENML_UNIT_GRAM_PER_LITER:               return "g/l";

    case SENML_UNIT_DEGREE_PER_SECOND:            return "deg/s";
    case SENML_UNIT_COUNT_PER_CUBIC_METER:        return "count/m3";
    case SENML_UNIT_DAY:                          return "day";
    case SENML_UNIT_MONTH:                        return "month";
    case SENML_UNIT_YEAR:                         return "year";

    default:                                      return "";
    }
}

size_t senml_saul_encode_cbor(uint8_t *buf, size_t len)
{
    phydat_t data;
    senml_value_t val;
    senml_bool_t val_b;
    nanocbor_encoder_t enc;
    saul_reg_t *dev = saul_reg;

    nanocbor_encoder_init(&enc, buf, len);
    nanocbor_fmt_array_indefinite(&enc);

    while (dev) {
        int dim = saul_reg_read(dev, &data);
        if (dim <= 0) {
            return 0;
        }

        for (uint8_t i = 0; i < dim; i++) {
            switch (data.unit) {
            case UNIT_BOOL:
                phydat_to_senml_bool(&val_b, &data, i, dev->name, 0);
                senml_encode_bool_cbor(&enc, &val_b);
                break;
            case UNIT_TIME:
                phydat_time_to_senml_value(&val, &data, dev->name, 0);
                senml_encode_value_cbor(&enc, &val);
                break;
            default:
                phydat_to_senml_value(&val, &data, i, dev->name, 0);

                // Fix the unit for relative humidity.
                if (dev->driver->type == SAUL_SENSE_HUM &&
                    val.unit == SENML_UNIT_PERCENT) {
                    val.unit = SENML_UNIT_RELATIVE_HUMIDITY_PERCENT;
                }

                senml_encode_value_cbor(&enc, &val);
                break;
            }
        }

        dev = dev->next;
    }

    nanocbor_fmt_end_indefinite(&enc);
    return nanocbor_encoded_len(&enc);
}

static void senml_encode_start_cbor(nanocbor_encoder_t *enc, const char *name,
                                    float age, uint8_t unit)
{
    // Start map
    nanocbor_fmt_map(enc, 1 + (name != NULL) + (age != 0)
                     + (unit != SENML_UNIT_NONE));

    // Name
    if (name != NULL) {
        nanocbor_fmt_int(enc, SENML_LABEL_NAME);
        nanocbor_put_tstr(enc, name);
    }

    // Relative timestamp
    if (age) {
        nanocbor_fmt_int(enc, SENML_LABEL_TIME);
        nanocbor_fmt_float(enc, age);
    }

    // Unit
    if (unit != SENML_UNIT_NONE) {
        nanocbor_fmt_int(enc, SENML_LABEL_UNIT);
        nanocbor_put_tstr(enc, senml_unit_to_str(unit));
    }
}

void senml_encode_bool_cbor(nanocbor_encoder_t *enc, const senml_bool_t *val)
{
    // Start and encode common properties
    senml_encode_start_cbor(enc, val->name, val->age, val->unit);

    // Encode value
    nanocbor_fmt_int(enc, SENML_LABEL_BOOLEAN_VALUE);
    nanocbor_fmt_bool(enc, val->value);
}

void senml_encode_value_cbor(nanocbor_encoder_t *enc, const senml_value_t *val)
{
    if (isnan(val->value)) {
        return;
    }

    // Start and encode common properties
    senml_encode_start_cbor(enc, val->name, val->age, val->unit);

    // Encode value
    nanocbor_fmt_int(enc, SENML_LABEL_VALUE);
    nanocbor_fmt_float(enc, val->value);
}
