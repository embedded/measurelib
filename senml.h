#pragma once

#include "nanocbor/nanocbor.h"
#include "phydat.h"

typedef enum {
    SENML_LABEL_BASE_VERSION    = -1,
    SENML_LABEL_BASE_NAME       = -2,
    SENML_LABEL_BASE_TIME       = -3,
    SENML_LABEL_BASE_UNIT       = -4,
    SENML_LABEL_BASE_VALUE      = -5,
    SENML_LABEL_BASE_SUM        = -6,
    SENML_LABEL_NAME            = 0,
    SENML_LABEL_UNIT            = 1,
    SENML_LABEL_VALUE           = 2,
    SENML_LABEL_STRING_VALUE    = 3,
    SENML_LABEL_BOOLEAN_VALUE   = 4,
    SENML_LABEL_SUM             = 5,
    SENML_LABEL_TIME            = 6,
    SENML_LABEL_UPDATE_TIME     = 7,
    SENML_LABEL_DATA_VALUE      = 8,
} senml_label_t;

typedef enum {
    /* SenML units from RFC8428 */
    SENML_UNIT_NONE,
    SENML_UNIT_METER,
    SENML_UNIT_KILOGRAM,
    SENML_UNIT_GRAM,
    SENML_UNIT_SECOND,
    SENML_UNIT_AMPERE,
    SENML_UNIT_KELVIN,
    SENML_UNIT_CANDELA,
    SENML_UNIT_MOLE,
    SENML_UNIT_HERTZ,
    SENML_UNIT_RADIAN,
    SENML_UNIT_STERADIAN,
    SENML_UNIT_NEWTON,
    SENML_UNIT_PASCAL,
    SENML_UNIT_JOULE,
    SENML_UNIT_WATT,
    SENML_UNIT_COULOMB,
    SENML_UNIT_VOLT,
    SENML_UNIT_FARAD,
    SENML_UNIT_OHM,
    SENML_UNIT_SIEMENS,
    SENML_UNIT_WEBER,
    SENML_UNIT_TESLA,
    SENML_UNIT_HENRY,
    SENML_UNIT_CELSIUS,
    SENML_UNIT_LUMEN,
    SENML_UNIT_LUX,
    SENML_UNIT_BECQUEREL,
    SENML_UNIT_GRAY,
    SENML_UNIT_SIEVERT,
    SENML_UNIT_KATAL,
    SENML_UNIT_SQUARE_METER,
    SENML_UNIT_CUBIC_METER,
    SENML_UNIT_LITER,
    SENML_UNIT_METER_PER_SECOND,
    SENML_UNIT_METER_PER_SQUARE_SECOND,
    SENML_UNIT_CUBIC_METER_PER_SECOND,
    SENML_UNIT_LITER_PER_SECOND,
    SENML_UNIT_WATT_PER_SQUARE_METER,
    SENML_UNIT_CANDELA_PER_SQUARE_METER,
    SENML_UNIT_BIT,
    SENML_UNIT_BIT_PER_SECOND,
    SENML_UNIT_LATITUDE,
    SENML_UNIT_LONGITUDE,
    SENML_UNIT_PH,
    SENML_UNIT_DECIBEL,
    SENML_UNIT_DBW,
    SENML_UNIT_BEL,
    SENML_UNIT_COUNT,
    SENML_UNIT_RATIO,
    SENML_UNIT_RATIO_2,
    SENML_UNIT_RELATIVE_HUMIDITY_PERCENT,
    SENML_UNIT_REMAINING_BATTERY_PERCENT,
    SENML_UNIT_REMAINING_BATTERY_SECONDS,
    SENML_UNIT_RATE,
    SENML_UNIT_RPM,
    SENML_UNIT_HEART_RATE,
    SENML_UNIT_HEART_BEATS,
    SENML_UNIT_CONDUCTIVITY,
    SENML_UNIT_SIEMENS_PER_METER,

    /* SenML units from RFC8798 */
    SENML_UNIT_BYTE,
    SENML_UNIT_VOLT_AMPERE,
    SENML_UNIT_VOLT_AMPERE_SECOND,
    SENML_UNIT_VOLT_AMPERE_REACTIVE,
    SENML_UNIT_VOLT_AMPERE_REACTIVE_SECOND,
    SENML_UNIT_JOULE_PER_METER,
    SENML_UNIT_KILOGRAM_PER_CUBIC_METER,
    SENML_UNIT_DEGREE,

    /* SenML units from ISO7027-1:2016 */
    SENML_UNIT_NEPHELOMETRIC_TURBIDITY_UNIT,

    /* SenML secondary units from RFC8798 */
    SENML_UNIT_MILLISECOND,
    SENML_UNIT_MINUTE,
    SENML_UNIT_HOUR,
    SENML_UNIT_MEGAHERTZ,
    SENML_UNIT_KILOWATT,
    SENML_UNIT_KILOVOLT_AMPERE,
    SENML_UNIT_KILOVAR,
    SENML_UNIT_AMPERE_HOUR,
    SENML_UNIT_WATT_HOUR,
    SENML_UNIT_KILOWATT_HOUR,
    SENML_UNIT_VAR_HOUR,
    SENML_UNIT_KILOVAR_HOUR,
    SENML_UNIT_KILOVOLT_AMPERE_HOUR,
    SENML_UNIT_WATT_HOUR_PER_KILOMETER,
    SENML_UNIT_KIBIBYTE,
    SENML_UNIT_GIGABYTE,
    SENML_UNIT_MEGABIT_PER_SECOND,
    SENML_UNIT_BYTE_PER_SECOND,
    SENML_UNIT_MEGABYTE_PER_SECOND,
    SENML_UNIT_MILLIVOLT,
    SENML_UNIT_MILLIAMPERE,
    SENML_UNIT_DECIBEL_MILLIWATT,
    SENML_UNIT_MICROGRAM_PER_CUBIC_METER,
    SENML_UNIT_MILLIMETER_PER_HOUR,
    SENML_UNIT_METER_PER_HOUR,
    SENML_UNIT_PARTS_PER_MILLION,
    SENML_UNIT_PERCENT,
    SENML_UNIT_PERMILLE,
    SENML_UNIT_HECTOPASCAL,
    SENML_UNIT_MILLIMETER,
    SENML_UNIT_CENTIMETER,
    SENML_UNIT_KILOMETER,
    SENML_UNIT_KILOMETER_PER_HOUR,

    /* SenML secondary units from CoRE-1 */
    SENML_UNIT_PARTS_PER_BILLION,
    SENML_UNIT_PARTS_PER_TRILLION,
    SENML_UNIT_VOLT_AMPERE_HOUR,
    SENML_UNIT_MILLIGRAM_PER_LITER,
    SENML_UNIT_MICROGRAM_PER_LITER,
    SENML_UNIT_GRAM_PER_LITER,

    /* SenML units for RIOT compatibility */
    SENML_UNIT_DEGREE_PER_SECOND,
    SENML_UNIT_COUNT_PER_CUBIC_METER,
    SENML_UNIT_DAY,
    SENML_UNIT_MONTH,
    SENML_UNIT_YEAR,
} senml_unit_t;

typedef struct {
    const char *name;
    float age;
    float value;
    uint8_t unit;
} senml_value_t;

typedef struct {
    const char *name;
    float age;
    bool value;
    uint8_t unit;
} senml_bool_t;

void phydat_time_to_senml(senml_value_t *senml, const phydat_t *phydat,
                          const char *name);

void phydat_to_senml_bool(senml_bool_t *senml, const phydat_t *phydat,
                          const uint8_t dim, const char *name, float age);

void phydat_to_senml_value(senml_value_t *senml, const phydat_t *phydat,
                           const uint8_t dim, const char *name, float age);

const char *senml_unit_to_str(uint8_t unit);

size_t senml_saul_encode_cbor(uint8_t *buf, size_t len);

void senml_encode_bool_cbor(nanocbor_encoder_t *enc, const senml_bool_t *val);
void senml_encode_value_cbor(nanocbor_encoder_t *enc, const senml_value_t *val);

void encode_measurement_value_cbor(nanocbor_encoder_t *enc, const char *name,
                                   float age, float value, const char *unit);
